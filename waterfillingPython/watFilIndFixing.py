import sys
import copy
TIMESTEPS = 365 * 2 #* 12
STEPSIZE = 43200#/12  #in seconds
CAPACITY = 62500 #joules
#read the the data TODO allong with 
dataFile = "exampl1Bidaily.csv"
#read the energy data:
fin = open(dataFile, 'r')
energyavail = []
for line in fin:
    energyavail = energyavail + [line.strip().split(',')]
fin.close()
for i in range(len(energyavail)):
    for j in range(11):
        energyavail[i][j]=STEPSIZE*float(energyavail[i][j])#till here

def fixingPaths(fixed,k):#if a node nearer the sink cannot send more data, the nodes farther away cannot route anything more through it
    node = k+1
    if node == 6:
        fixed[6]=True
        fixed[9]=True
        fixed[8]=True
    if node == 2:
        fixed[0]=True
        fixed[2]=True
        fixed[3]=True
    if node == 8:
        fixed[10]=True
        fixed[4]=True
    if node == 7:
        fixed[9]=True
        fixed[8]=True 
    if node == 1:
        fixed[2]=True
        fixed[3]=True
    if node == 11:
        fixed[4]=True
    if node == 10:
        fixed[8]=True
    if node == 3:
        fixed[3]=True
    return

def batteryUpdate(node, rate): #this needs to change according to all rates
#4 floor 
    sending = 1.31 #q
    recei = 1 #p 
    node = node+1
    if node == 6:
        return sending*(rate[5]+rate[6]+rate[9]+rate[8])+ recei*(rate[6]+rate[9]+rate[8]) 
    if node == 2:
        return sending*(rate[1]+rate[0]+rate[2]+rate[3])+ recei*(rate[0]+rate[2]+rate[3])
    if node == 8:
        return sending*(rate[7]+rate[10]+rate[4])+ recei*(rate[10]+rate[4]) 
    if node == 7:
        return sending*(rate[6]+rate[9]+rate[8])+ recei*(rate[9]+rate[8]) 
    if node == 1:
        return sending*(rate[0]+rate[2]+rate[3])+ recei*(rate[2]+rate[3])
    if node == 11:
        return sending*(rate[10]+rate[4])+ recei*(rate[4]) 
    if node == 10:
        return sending*(rate[9]+rate[8])+ recei*(rate[8]) 
    if node == 3:
        return sending*(rate[2]+rate[3])+ recei*(rate[3])
    if node == 5:
        return sending*(rate[4]) 
    if node == 9:
        return sending*(rate[8])
    if node == 4:
        return sending*(rate[3])

def recalBatery(battery, energyavail,rate,fixed,incr):
    for i in range(TIMESTEPS):
        for k in range(11):
            enerspen = batteryUpdate(k, rate[i])#this still needs to get the rate for all of em TODO
            bat = battery[i][k] + energyavail[i][k] 
            battery[i+1][k]= min(bat, CAPACITY) - enerspen
            if battery[i+1][k] < 0:
                if incr < 0.0000005:
                    g = i
                    while g > -1:
                        if fixed[g][k] == False:
                            fixed[g][k] = True
                            fixingPaths(fixed[g],k)
                            break
                        else:
                            g= g-1                    
                return True
    return False

battery =[[0,0,0,0,0,0,0,0,0,0,0]]
fixed =[]
rate=[]
#initialize the tables (battery, energyavail, fixed, Rate for sensor at timestep.)
for i in range(TIMESTEPS):
    fixed.append([False,False,False,False,False,False,False,False,False,False,False])
    rate.append([0,0,0,0,0,0,0,0,0,0,0])
    temp=[]
    for k in range(11):
        bat =  energyavail[i][k] +battery[i][k] 
        bat = min(bat, CAPACITY)
        temp.append(bat)
    #print(temp)
    battery.append(temp)
    
        #count=0
        #for j in fixed:
            #if j:
                #count=count+1
        #print(count) 


#main loop, increment slowly the rate to try to maximize the rate for everything
#then fix the rates that cannot be increased.
staRate = 10000.0
stop = True

while stop:
#copy the rate list to another list
    tempRate = copy.deepcopy(rate)
    #increment by rate/find the rate
    for i in range(TIMESTEPS):
        for k in range(11):
            if fixed[i][k] == False:
                tempRate[i][k] = tempRate[i][k] + staRate
    #test everything working properly by recalculating the bat
    flag = recalBatery(battery,energyavail,tempRate,fixed,staRate)

    if flag == True:
        if staRate < 0.0000005:
            staRate= 1000.0
        staRate = staRate/2
        #print(staRate)
        #sys.stdout.flush()  
    else:
        #TODO
        #print(staRate)
        for i in range(TIMESTEPS):
            for k in range(11):
                if fixed[i][k] == False:
                    rate[i][k] = rate[i][k] + staRate        
        #fix the rates that need to be fixed
        for i in range(TIMESTEPS):
            for k in range(11):
                if fixed[i][k] == False and battery[i+1][k] < 0.0005:
                    fixed[i][k] = True
                    fixingPaths(fixed[i],k)
                    staRate = 1000.0
                    g = i
                    while g > -1:
                        if battery[g][k] + energyavail[g][k] -batteryUpdate(k, rate[g]) - CAPACITY> 0.0005:
                            break
                        else:
                            fixed[g][k] = True
                            fixingPaths(fixed[g],k)
                        g= g-1
        #if all rates are fixed stop = False
        stop = False
        for i in range(TIMESTEPS):
            for k in range(11):        
                if False == fixed[i][k]:
                    stop = True
        #count=0
        #for j in fixed:
            #if j:
                #count=count+1
        #print(count) 
        #print(staRate)

resultFile = open("waterfillinresBidex3.csv", 'w')
#after the loop calculate all the rates
for i in range(TIMESTEPS):
    for k in range(11): 
        resultFile.write(str(rate[i][k])+',')
    resultFile.write('0\n')
resultFile.close()

resultFile = open("trueFAlseHour.csv", 'w')
#after the loop calculate all the rates
for i in range(TIMESTEPS):
    resultFile.write(str(fixed[i])+'\n')
resultFile.close()


recalBatery(battery,energyavail,rate,fixed,100)
resultFile = open("batex3Bid.csv", 'w')
#after the loop calculate all the rates
for i in range(TIMESTEPS):
    for k in range(11):
        resultFile.write(str(battery[i][k])+',')
    resultFile.write('\n')
resultFile.close()
