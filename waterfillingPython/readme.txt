2 python files and data.
watFil.py is the water filling with how it would work if you want allthe nodes to send the exact amount on every turn. 

watFilIndFixing.py fixes individual node rates, which means that in the same cycle, one node can send more data than another. The nodes will never send more of their data in the expense of another node that has a smaller rate, as they will only increase the rate after that node has fixed his.
