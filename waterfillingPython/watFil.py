import sys

TIMESTEPS = 365 * 2
STEPSIZE = 43200  #in seconds
CAPACITY = 62500 #joules
#read the the data TODO allong with 
dataFile = "exampl1Bidaily.csv"
#read the energy data:
fin = open(dataFile, 'r') #TODO
energyavail = []
for line in fin:
    energyavail = energyavail + [line.strip().split(',')]
fin.close()
for i in range(len(energyavail)):
    for j in range(11):
        energyavail[i][j]=STEPSIZE*float(energyavail[i][j])#till here

def batteryUpdate(node, rate): #this should be able 
#4 floor
    sending = 1.31 #q
    recei = 1 #p 
    node = node+1
    if node in [6,2]:
        return sending*4*rate + recei*3*rate
    if node in [8,7,1]:
        return sending*3*rate + recei*2*rate
    if node in [11,10,3]:
        return sending*2*rate + recei*rate
    if node in [5,9,4]:
        return sending*rate

def recalBatery(battery, energyavail,rate):
    for i in range(TIMESTEPS):
        for k in range(11):
            enerspen = batteryUpdate(k, rate[i])
            bat = battery[i][k] + energyavail[i][k] 
            battery[i+1][k]= min(bat, CAPACITY) - enerspen
            if battery[i+1][k] < 0:
                print(i)
                return True
    return False

battery =[[0,0,0,0,0,0,0,0,0,0,0]]
fixed =[]
rate=[]
#initialize the tables (battery, energyavail, fixed, Rate for sensor at timestep.)
for i in range(TIMESTEPS):
    fixed.append(False)
    rate.append(0)
    temp=[]
    for k in range(11):
        bat =  energyavail[i][k] +battery[i][k] 
        bat = min(bat, CAPACITY)
        temp.append(bat)
    #print(temp)
    battery.append(temp)
    


#main loop, increment slowly the rate to try to maximize the rate for everything
#then fix the rates that cannot be increased.
staRate = 10000.0
stop = True

while stop:
#copy the rate list to another list
    tempRate = list(rate)
    #increment by rate/find the rate
    for i in range(TIMESTEPS):
        if fixed[i] == False:
            tempRate[i] = tempRate[i] + staRate
    #test everything working properly by recalculating the bat
    flag = recalBatery(battery,energyavail,tempRate)

    if flag == True:
        if staRate < 0.000000000005:
            print("staRate cannot be decreased")
            break
        staRate = staRate/2
        #print(staRate)
        #sys.stdout.flush()  
    else:
        #TODO
        #print(staRate)
        for i in range(TIMESTEPS):
            if fixed[i] == False:
                rate[i] = rate[i] + staRate        
        #fix the rates that need to be fixed
        for i in range(TIMESTEPS):
            for k in range(11):
                if fixed[i] == False and battery[i+1][k] < 0.0005:
                    fixed[i] = True
                    staRate = 1000.0
                    g = i
                    while g > -1:
                        if battery[g][k] + energyavail[g][k] -batteryUpdate(k, rate[g]) - CAPACITY> 0.0005:
                            break
                        else:
                            fixed[g] = True
                        g= g-1
        #if all rates are fixed stop = False
        if False not in fixed:
            stop = False
        count=0
        for j in fixed:
            if j:
                count=count+1
        print(count) 
        #print(staRate)

resultFile = open("waterfillingresBidailyex3.csv", 'w')
#after the loop calculate all the rates
for i in range(TIMESTEPS):
    resultFile.write(str(rate[i])+'\n')
resultFile.close()

resultFile = open("trueFAlse.csv", 'w')
#after the loop calculate all the rates
for i in range(TIMESTEPS):
    resultFile.write(str(fixed[i])+'\n')
resultFile.close()


recalBatery(battery,energyavail,rate)
resultFile = open("batex3.csv", 'w')
#after the loop calculate all the rates
for i in range(TIMESTEPS):
    for k in range(11):
        resultFile.write(str(battery[i][k])+',')
    resultFile.write('\n')
resultFile.close()
