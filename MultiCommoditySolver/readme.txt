Multipleexperiments.py is the file that runs the experiments with the help of python files datawr (fair multicommodity), datawrCap (fair multi with specific capacitance), dataSum (maximum multicommodity) and dataSumCap (maximum multicommodity with capacitance)

The system uses lpsolve.exe on windows, by building the whole system of equations and then running lpsolve, to solve them for every timeslot.
Graphtetarto.txt is the file that has the graph skeleton of all the connections of the network with the sink being in the fourth flour

exampl*.csv are the data files. number means the year that the data are from
solutions are in the two subfolders, one without cutting anything from the final solution (1) and the other with a small 20% reserve (0.8)
