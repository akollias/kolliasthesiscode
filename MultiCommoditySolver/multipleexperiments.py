import datawr
import datawrCap
import dataSum
import dataSumCap #this is the maximum sum with capacity. Was not used at the end
#import tCreateFl
import os
import sys

os.chdir(os.path.dirname(sys.argv[0]))
percent = [1]
#graph = ['deutero','tetarto'] #given to all functions so they can open the correct files

graph = ['tetarto'] #given to all functions so they can open the correct files
#ratios = [ 5.11]
#percent = [0.8,1]

ratios = [1.31] #different ratios
#ratios = [6.20]
#filenames= ['Weekly','Monthly']
#timep =[604800,2592000] 
filenames= ['Hour','Bidaily','Daily','Weekly','Monthly']
timep =[3600,43200,86400,604800,2592000] #seconds between timepoints
for graphic in graph:
    general = open("genresult"+graphic+".txt",'w')
    for per in percent:
        for j in ratios:
            for i in range(len(filenames)):
                temp = j*10
                temp = str(temp)
                for year in range(1,3):
                    if ((year == 1) and (i == 0)):
                        nothing=0
                    else:
                        if ((year == 2) and (i == 0)):
                            nothing=0
                        else:
                            d = datawr.myFunction(graphic,year,j,filenames[i],temp[:2],timep[i],per) #fair multicommodity
                            general.write(str(per)+temp[:2]+filenames[i]+"res"+str(year) + " = " +str(d) +'\n')
                        d = dataSum.myFunction(graphic,year,j,filenames[i],temp[:2],timep[i],per) #maximum multicommodity
                        general.write(str(per)+temp[:2]+filenames[i]+"sumres"+str(year) + " = " +str(d) +'\n')
                        #d = dataSumCap.myFunction(graphic,year,j,filenames[i],temp[:2],62500,timep[i],per)#put specific number capacitor
                        #general.write(str(per)+temp[:2]+filenames[i]+"sumcapres"+str(year) + " = " +str(d))
                        d = datawrCap.myFunction(graphic,year,j,filenames[i],temp[:2],62500,timep[i],per)#capacitor is 2.5F 5 V Fair multicommodity with capacitance
                        general.write(str(per)+temp[:2]+filenames[i]+"capres"+str(year) + " = " +str(d) +'\n')
    general.close()
    per = 0.8
    j= 2.12
    i = 0
    temp = j*10
    temp = str(temp)
    year=2
    dataSum.myFunction(graphic,year,j,filenames[i],temp[:2],timep[i],per)
#datawr.myFunction(2.11678832117,'Yearly','21',31536000)
#dataSum.myFunction(2.11678832117,'Yearly','21',31536000)
#tCreateFl.myFunction(2.11678832117,'daily','21',86400)
