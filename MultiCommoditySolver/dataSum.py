import os
def myFunction(graphic,year,qu,filena,postfix,timeP,percent1):
    accumul = 0
    #fin = open("measurements.txt", 'r')
    #fin = open("fortcapa.txt", 'r')
    fin = open("graph"+graphic+".txt", 'r')
    
    
    #results
    resultFile = open(graphic+str(percent1)+"/"+postfix+filena+"resSum"+str(year)+".csv", 'w')
    energFile = open(graphic+str(percent1)+"/"+postfix+filena+"energyleftSum"+str(year)+".csv", 'w')
    dataFile = "exampl"+str(year)+filena+".csv"
    #dataFile='example.csv'
    q = float(fin.readline().strip().split()[0])
    #q = 1.31386861314
    q = qu
    p = float(fin.readline().strip().split()[0])
    nodeNo = int(fin.readline().strip())
    
    curlistall = []
    alEdges = []
    allFlows = []
    equations = []
    #thelo to pinaka
    
    for line in range(0,nodeNo):
        temp = fin.readline().strip().split()
        curlistall = curlistall + [(temp[0],temp[1:])]
        
    for line in range(0,nodeNo):
        temp = fin.readline().strip().split()
        curlistall[line] = curlistall[line] + (temp[1],)
    
    
    fin.close()
    
    #read the energy data:
    fin = open(dataFile, 'r') #TODO
    energyAv = []
    for line in fin:
        energyAv = energyAv + [line.strip().split(',')]
    fin.close()
    for i in range(len(energyAv)):
        for j in range(nodeNo):
            energyAv[i][j] = float(energyAv[i][j]) #maybe add the data here TODO.
    left = []
    for j in range(nodeNo):
        left = left + [0]
    #till here
    
    for i in curlistall:
        for j in i[1]:
            alEdges = alEdges + [(i[0],j)]
    
    #allflows
    for i in curlistall:
        for j in alEdges:
            allFlows = allFlows + [(i[0],j)]
    #print(len(allFlows))
    #for i in allFlows:
        #print(i)
    ##we have all the edges on the alEdges. now i need all flows all edges, and then i need the end result
    #print(allFlows[0])
    #print(curlistall[0])
    
    
    #first number of equations
    for i in range(0,nodeNo):
        tempo = ''
        for j in allFlows:
            if curlistall[i][0] == j[0]:
                if j[1][0] == curlistall[i][0]:
                    tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                if j[1][1] == 't':
                    tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                
        equations = equations + [tempo + '= 0;\n']
    
    #second equations
    for i in range(0,nodeNo): #flow i
        
        for k in range(0,nodeNo): #node k
            if i != k:
                tempo = ''
                for j in allFlows:
                    if curlistall[i][0] == j[0]:#we are talking about flow i
                        if j[1][0] == curlistall[k][0]: #inbound to k
                            tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                        elif j[1][1] == curlistall[k][0]: #outbound of k
                            tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                        
                equations = equations + [tempo + '= 0;\n']   
        
    
    ##equality equations for the target maximization
    #for i in range(0,nodeNo): #flow i
        
        #for k in range(i,nodeNo): #flow k
            #if i != k:
                #tempo = ''
                #for j in allFlows:
                    #if curlistall[i][0] == j[0]:
                        #if j[1][0] == curlistall[i][0]:
                            #tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                    #elif curlistall[k][0] == j[0]:
                        #if j[1][0] == curlistall[k][0]:
                            #tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                        
                #equations = equations + [tempo + '= 0;\n']
    ##the end
    #for i in range(0,nodeNo): #flow i
        
        #for k in range(i,nodeNo): #flow k
            #if i != k:
                #tempo = ''
                #for j in allFlows:
                    #if curlistall[i][0] == j[0]:
                        #if j[1][1] == 't':
                            #tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                    #elif curlistall[k][0] == j[0]:
                        #if j[1][1] == 't':
                            #tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                        
                #equations = equations + [tempo + '= 0;\n']
    
    #for i in range(0,nodeNo): #flow i
        #for k in range(i,nodeNo): #flow k
            #if i != k:
                #tempo = ''
                #for j in allFlows:
                    #if curlistall[i][0] == j[0]:
                        #if j[1][0] == curlistall[i][0]:
                            #tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                    #elif curlistall[k][0] == j[0]:
                        #if j[1][1] == 't':
                            #tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                            
                #equations = equations + [tempo + ' = 0;\n']
    
    #done with equations TODO FOR CHANGES #add the minimization of the cost
    #listFile = open("listOfnames.txt", 'w')
    for ind in range(len(energyAv)):
        currentEnerg = energyAv[ind]
        costCon = []
        minimizationFunc = '' #this is the second lp we run to minimize the cost  #TAGMIN
        #3rd equations - constraints. this stays as is
        for i in range(0,nodeNo): #node i
            tempo = ''
            for j in allFlows:
                if j[1][0] == curlistall[i][0]: #outbound to i
                    tempo = tempo + '+'+str(q) +' f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                elif j[1][1] == curlistall[i][0]: #inbound of i
                    tempo = tempo + '+'+str(p) +' f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                    
            left[i] = left[i] + currentEnerg[i]*timeP #TODO add multiplication for data
            minimizationFunc = minimizationFunc +" + " + tempo#TAGMIN
            costCon = costCon + ['sen'+str(i)+': '+tempo + ' <= '+ str(left[i]) +';\n']
        #till here ebergyAv  instead of curlistall
        temLeft = []
        for curLEft in left:
            temLeft = temLeft + [str(curLEft)]
        energFile.write(";".join(temLeft) + '\n')
          
        #for i in range(len(equations)):
            #for j in range(len(equations[i][0])):
                #equations[i][0][j] = str(equations[i][0][j])
        
        
        
        
        fout= open("simp.lp", 'w')
        #maximize
        #tempo = ''
        #for j in allFlows:
            #if curlistall[0][0] == j[0]:
                #if j[1][0] == curlistall[0][0]:
                    #tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
        
        #word = tempo[:-1] +";\n"
        #fout.write(word)
        
        #sum
        tempo = ''
        for i in range(0,nodeNo): #flow i
            for j in allFlows:
                if curlistall[i][0] == j[0]:
                    if j[1][0] == curlistall[i][0]:
                        tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
            
        word = tempo[:-1] +";\n"
        fout.write(word)
        newCon = tempo[:-1] +" = " #TAGMIN
        
        for i in equations:
            word = i
            fout.write(word)
        
        for i in costCon:
            word = i
            fout.write(word)
            
        for j in allFlows:
            word='f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' >= 0;\n'
            fout.write(word)
        
        
        
        
        
        #what to maximize:
        #num = []
        #alFl = ''
        #for j in allFlows:
            #alFl= alFl+'f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+', '
            #num = num + ['0']
        
        #word = 'int ' + alFl[:-2] + ";\n"     
        #fout.write(word)
        #no ints
        
        
        fout.close()
        #filename = str(ind)+'.res'
        filename = 'demires.res'
        os.system('lp_solve.exe -s0 -S3 simp.lp > '+filename)
        
        #read the .res file and find the data send, rewrite the simp.lp and then new sol -> substract from energy left and add all the data to the (resultFile)res.csv file also add the filename to listfile
        #TAGMIN
        tempSol = 0
        tefin = open(filename,'r')
        for line in tefin:
            if line[:3] == 'Val':
                agf = line.strip().split()
                tempSol=float(agf[4])                
                agf[4]=str( float(agf[4])*percent1   )#PERCENT
                accumul = accumul +float(agf[4]) 
                resultFile.write(agf[4]+'\n')
        tefin.close()        
        newCon = newCon + str(tempSol)+ ';\n'
        
        #pause here
        fout= open("simpm.lp", 'w')
        word = minimizationFunc +";\n"
        fout.write(word)
        for i in equations:
            word = i
            fout.write(word)
        
        for i in costCon:
            word = i
            fout.write(word)
            
        for j in allFlows:
            word='f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' >= 0;\n'
            fout.write(word)
        word = newCon
        fout.write(word)
        fout.close()
        os.system('lp_solve.exe -min -s0 -S3 simpm.lp > '+filename)
        
        #TAGMIN till here
        flag = 0
        # we rerun the lp
        tefin = open(filename,'r')
        for line in tefin:
            if line[0:3] == 'Thi':
                print(graphic,year,qu,filena,postfix,timeP,percent1)
                flag = 1

            if line[0:3] == 'sen':
                if(line[4] == ':'):
                    g = int(line[3])
                else:
                    g = int(line[3:5])
                asd = line.strip().split()
                asd = float(asd[1])*percent1 #subtract from left#PERCENT
                left[g]=left[g] - asd
                #check the number take the energy
        tefin.close()
        if flag == 1:
            os.system('lp_solve.exe -s0 -S3 simp.lp > '+filename)
            flag=0
            tefin = open(filename,'r')
            for line in tefin:
                if line[0:3] == 'sen':
                    if(line[4] == ':'):
                        g = int(line[3])
                    else:
                        g = int(line[3:5])
                    asd = line.strip().split()
                    asd = float(asd[1])*percent1 #subtract from left#PERCENT
                    
                    left[g]=left[g] - asd
                    #check the number take the energy
                    '''
                
                if line[0] == 'f':
                    agf = line.strip().split()
                    asd = agf[0].split('s')
                    if asd[1] in curDic:
                    
                    else:
                    curDic = {}
                    
                    '''
            tefin.close() 
        #listFile.write(filename+'\n')
    
    
    
    
    energFile.close()
    resultFile.close()
    #listFile.close()
    return accumul