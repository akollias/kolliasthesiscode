import os
def myFunction(qu,filena,postfix,timeP):

    #fin = open("measurements.txt", 'r')
    #fin = open("fortcapa.txt", 'r')
    fin = open("graph.txt", 'r')
    
    
    #results
    resultFile = open("res"+filena+postfix+".csv", 'w')
    energFile = open("energyleft"+filena+postfix+".csv", 'w')
    dataFile = "exampl"+filena+".csv"
    #dataFile='example.csv'
    
    q = float(fin.readline().strip().split()[0])
    #q = 1.31386861314
    q = qu
    p = float(fin.readline().strip().split()[0])
    nodeNo = int(fin.readline().strip())
    
    curlistall = []
    alEdges = []
    allFlows = []
    equations = []
    #thelo to pinaka
    
    for line in range(0,nodeNo):
        temp = fin.readline().strip().split()
        curlistall = curlistall + [(temp[0],temp[1:])]
        
    for line in range(0,nodeNo):
        temp = fin.readline().strip().split()
        curlistall[line] = curlistall[line] + (temp[1],)
    
    
    fin.close()
    
    #read the energy data:
    fin = open(dataFile, 'r') #TODO
    energyAv = []
    for line in fin:
        energyAv = energyAv + [line.strip().split(',')]
    fin.close()
    for i in range(len(energyAv)):
        for j in range(nodeNo):
            energyAv[i][j] = float(energyAv[i][j]) #maybe add the data here TODO.
    left = []
    for j in range(nodeNo):
        left = left + [0]
    #till here
    
    for i in curlistall:
        for j in i[1]:
            alEdges = alEdges + [(i[0],j)]
    
    #allflows
    for i in curlistall:
        for j in alEdges:
            allFlows = allFlows + [(i[0],j)]
    print(len(allFlows))
    #for i in allFlows:
        #print(i)
    ##we have all the edges on the alEdges. now i need all flows all edges, and then i need the end result
    #print(allFlows[0])
    #print(curlistall[0])
    
    
    #first number of equations
    for i in range(0,nodeNo):
        tempo = ''
        for j in allFlows:
            if curlistall[i][0] == j[0]:
                if j[1][0] == curlistall[i][0]:
                    tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                if j[1][1] == 't':
                    tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                
        equations = equations + [tempo + '= 0;\n']
    
    #second equations
    for i in range(0,nodeNo): #flow i
        
        for k in range(0,nodeNo): #node k
            if i != k:
                tempo = ''
                for j in allFlows:
                    if curlistall[i][0] == j[0]:#we are talking about flow i
                        if j[1][0] == curlistall[k][0]: #inbound to k
                            tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                        elif j[1][1] == curlistall[k][0]: #outbound of k
                            tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                        
                equations = equations + [tempo + '= 0;\n']   
        
    
    #equality equations for the target maximization
    for i in range(0,nodeNo): #flow i
        
        for k in range(i,nodeNo): #flow k
            if i != k:
                tempo = ''
                for j in allFlows:
                    if curlistall[i][0] == j[0]:
                        if j[1][0] == curlistall[i][0]:
                            tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                    elif curlistall[k][0] == j[0]:
                        if j[1][0] == curlistall[k][0]:
                            tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                        
                equations = equations + [tempo + '= 0;\n']
    #the end
    for i in range(0,nodeNo): #flow i
        
        for k in range(i,nodeNo): #flow k
            if i != k:
                tempo = ''
                for j in allFlows:
                    if curlistall[i][0] == j[0]:
                        if j[1][1] == 't':
                            tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                    elif curlistall[k][0] == j[0]:
                        if j[1][1] == 't':
                            tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                        
                equations = equations + [tempo + '= 0;\n']
    
    for i in range(0,nodeNo): #flow i
        for k in range(i,nodeNo): #flow k
            if i != k:
                tempo = ''
                for j in allFlows:
                    if curlistall[i][0] == j[0]:
                        if j[1][0] == curlistall[i][0]:
                            tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                    elif curlistall[k][0] == j[0]:
                        if j[1][1] == 't':
                            tempo = tempo + '-f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                            
                equations = equations + [tempo + ' = 0;\n']
    
    #done with equations TODO FOR CHANGES
    listFile = open("listOfnames.txt", 'w')
    #curDic = {}
    for ind in range(len(energyAv)):
        currentEnerg = energyAv[ind]
        costCon = []
        #3rd equations - constraints.
        for i in range(0,nodeNo): #node i
            tempo = ''
            for j in allFlows:
                if j[1][0] == curlistall[i][0]: #outbound to i
                    tempo = tempo + '+'+str(q) +' f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                elif j[1][1] == curlistall[i][0]: #inbound of i
                    tempo = tempo + '+'+str(p) +' f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
                    
            left[i] = left[i] + currentEnerg[i]*timeP #TODO add multiplication for data
            
            costCon = costCon + ['sen'+str(i)+': '+tempo + ' <= '+ str(left[i]) +';\n']
        #till here ebergyAv  instead of curlistall
        temLeft = []
        for curLEft in left:
            temLeft = temLeft + [str(curLEft)]
        energFile.write(";".join(temLeft) + '\n')
          
        #for i in range(len(equations)):
            #for j in range(len(equations[i][0])):
                #equations[i][0][j] = str(equations[i][0][j])
        
        
        
        
        fout= open("simp.lp", 'w')
        #maximize
        tempo = ''
        for j in allFlows:
            if curlistall[0][0] == j[0]:
                if j[1][0] == curlistall[0][0]:
                    tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
        
        word = tempo[:-1] +";\n"
        fout.write(word)
        
        #sum
        #tempo = ''
        #for i in range(0,nodeNo): #flow i
            #for j in allFlows:
                #if curlistall[i][0] == j[0]:
                    #if j[1][0] == curlistall[i][0]:
                        #tempo = tempo + '+f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' '
            
            #word = tempo[:-1] +";\n"
        #fout.write(word)
        
        
        for i in equations:
            word = i
            fout.write(word)
        
        for i in costCon:
            word = i
            fout.write(word)
            
        for j in allFlows:
            word='f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+' >= 0;\n'
            fout.write(word)
        
        
        
        
        
        #what to maximize:
        #num = []
        #alFl = ''
        #for j in allFlows:
            #alFl= alFl+'f'+str(j[0])+'s'+str(j[1][0])+'s'+str(j[1][1])+', '
            #num = num + ['0']
        
        #word = 'int ' + alFl[:-2] + ";\n"     
        #fout.write(word)
        #no ints
        
        
        fout.close()
        #filename = str(ind)+'.res'
        filename = 'demires.res'
        os.system('lp_solve.exe -s0 -S3 simp.lp > '+filename)
        #read the .res file, substract from energy left and add all the data to the (resultFile)res.csv file also add the filename to listfile
        curDic = {}
        tefin = open(filename,'r')
        for line in tefin:
            if line[:3] == 'Val':
                agf = line.strip().split()
                resultFile.write(agf[4]+'\n')
            if line[0:3] == 'sen':
                if(line[4] == ':'):
                    g = int(line[3])
                else:
                    g = int(line[3:5])
                asd = line.strip().split()
                asd = float(asd[1]) #subtract from left
                
                left[g]=left[g] - asd
                #check the number take the energy
                '''
                '''
            if line[0] == 'f':
                agf = line.strip().split()
                asd = agf[0].split('s')
                if asd[1] not in curDic:
                    curDic[asd[1]] = {}
                if asd[2] not in curDic[asd[1]]:
                    curDic[asd[1]][asd[2]] = 0
                curDic[asd[1]][asd[2]] = curDic[asd[1]][asd[2]] + float(agf[1])

        tefin.close()
        listFile.write(filename+'\n')
        #ds edo exoume to dictionary me ta data
        for asd in range(nodeNo):
            dictiFile = open('sen'+str(asd)+'ratios.csv','a')
            ld=list(curDic.items())
            temp=[]
            for agf in range(nodeNo):
                if str(agf+1) in curDic[str(asd+1)]:
                    temp = temp + [curDic[str(asd+1)][str(agf+1)]]
                else:
                    temp = temp + [0]
            if 't' in curDic[str(asd+1)]:
                temp = temp + [curDic[str(asd+1)]['t']]
            else:
                temp = temp + [0]            
                #curDic[str(asd+1)]  
            for dice in range(len(temp)):
                temp[dice]=str(temp[dice])
            dictiFile.write(','.join(temp)+'\n')
            dictiFile.close()
    #print(curDic)
    
    
    energFile.close()
    resultFile.close()
    listFile.close()