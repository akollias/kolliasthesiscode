function [oacc] = apall(filename,outf)
M= csvread(filename);
%this was used to find the minimum error in 
oacc =0
olda = 10000000;
n = 1;
k = 2;
l = 12;
fileID = fopen(outf,'w');
a = 24;
        %ToEstMdl = arima('ARLags',1:n,'MALags',1:k,'SMALags',1:l,'SARLags',1:a,'Seasonality',24);
        ToEstMdl = arima('Constant',0,'ARLags',[1 2 4],'MALags',1:5,'SMALags',[12 36 48 60],'SARLags',[12 24 48 72],'Seasonality',24);
	%ToEstMdl = arima(n,0,k);
    %ToEstMdl = arima('Constant',0,'ARLags',[1 2 3],'MALags',[2]);
%ToEstMdl = arima('Constant',0,'ARLags',[1 2 3 4],'MALags',[2]);    
    EstMdl = estimate(ToEstMdl,M(1:8760));

        pred=zeros(1,730);
	acc= zeros(1,12);
        for i=0:729;
		acc = forecast(EstMdl,12,'Y0',M(8760+i*12-100:8760+i*12));
		for lm = 1:12
		if acc(lm) < 0;
			acc(lm)=0;
end;
end;
            [pred(i+1)]=mean(acc);
        end;
        %acc = zeros(365,1);
        %for i=1:365;
        %    acc(i) = (pred(i) - M(i+365))*(pred(i) - M(i+365));
        %end;
        %oacc = sum(acc)/365
        for i=1:730;
		if pred(i) > 0;
            fprintf(fileID,'%.6f\n',pred(i));
        else
            fprintf(fileID,'%.6f\n',0);
		end;
        end;
fclose(fileID);

end