function [a] = runall()
%this run the code for all experiments. If you want to change the
%granularity, copy the corresponding files to the main folder and change
%the granularity numbers on the function files
a= zeros(1,11);
%for i=1
 for i=1:11
s = ['ap' int2str(i) '.csv'];
%s1= ['apres' int2str(i) '.csv'];
s1= ['exp' int2str(i) '.txt'];
[d]=apgrall(s,s1);
%[d]=apall(s,s1);
a(i)=d;
end
end