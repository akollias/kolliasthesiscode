function [olda] = apgrall(filename,outf)
M= csvread(filename);
fileID = fopen(outf,'w');
% This runs arima for each file (see line 21 for the factors) and saves the
% results. It finds the next number by using the previous 73 numbers
% to predict the new one. Writes the new results at outf and returns the
% MSE
fclose(fileID);
olda = 10000000;
numOfDatapoints = 8760;
pred=zeros(1,numOfDatapoints);
acc = zeros(1,numOfDatapoints);
% the for loops are for a grid search of which factors work the best
% for n = 1:5
% for k = 1:5
% for l = [12 24 36 48 60 72]
fileID = fopen(outf,'a');
% for a = [12 24 36 48 60 72]
    try
%         ToEstMdl = arima('ARLags',1:n,'MALags',1:k,'Seasonality',24);
%         ToEstMdl = arima('ARLags',1:n,'MALags',1:k,'SMALags',1:l,'SARLags',1:a,'Seasonality',24);
 ToEstMdl = arima('Constant',0,'ARLags',[1 2 4],'MALags',1:5,'SMALags',[12 36 48 60],'SARLags',[12 24 48 72],'Seasonality',24);       
%ToEstMdl = arima('Constant',0,'ARLags',1:n,'MALags',1:k,'SMALags',l,'SARLags',a,'Seasonality',24);
        EstMdl = estimate(ToEstMdl,M(1:numOfDatapoints)); %here is where the model is trained

        
%         
%         pred1=zeros(8760,1); This part forecasts for the second year
%         for i=0:8759;
%             [pred1(i+1)]=forecast(EstMdl,1,'Y0',M(8760+i-25:8760+i));
%         end;
%         acc = zeros(8760,1)
%         for i=1:8760;
%             acc(i) = (pred1(i) - M(i+8760))*(pred1(i) - M(i+8760));
%         end;
%         oacc = sum(acc)/8760

        %this part forecasts data of the first year. 
        for i=100:numOfDatapoints-1;
            [pred(i+1)]=forecast(EstMdl,1,'Y0',M(i-99:i)); %Estimates the next value
        end;
        for i=101:numOfDatapoints;
            acc(i) = (pred(i) - M(i))^2;
        end;
        olda = sum(acc)/numOfDatapoints-100;
%         if oacc < olda;
%         olda=oacc;
%         fprintf(fileID,'this is the new ol %.6f, n %.6f, k %.6f, l %.6f, a %.6f\n\t\r',olda,n,k,l,a);
%         fprintf('this is the new ol %.6f, n %.6f, k %.6f, l %.6f, a %.6f\n\t\r',olda,n,k,l,a);
%         end
    catch ME1
        fprintf('ante gia ol %.6f, n %.6f, k %.6f, l %.6f, a %.6f\n',olda,n,k,l,a);
        msgString = getReport(ME1);
        fprintf('%s\n\r',msgString)
    end
% end
fclose(fileID);
% end
% end
% end

end